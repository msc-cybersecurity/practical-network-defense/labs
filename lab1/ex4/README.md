# Lab01 Ex 04

## Activity 1

```bash
$ kathara lstart
$ cd ../../
$ ./connect-lab.sh 10.0.0.2/24
$ cd lab1/ex4
$ sudo tcpdump -i veth0 -w ex04_activity1_0.pcap
$ sudo tcpdump -i veth0 -w ex04_activity1_1.pcap
$ kathara lclean
$ sudo ip link del veth1 type veth
```

## Activity 2

```bash
$ sudo tcpdump -i wlp8s0 -w activity2_ftp.pcap
$ ftp test.rebex.net #demo:password
$ sudo tcpdump -i wlp8s0 -w activity2_sftp.pcap
$ sftp test.rebex.net #demo:password
```

Osservazioni: nella connessione sftp in Wireshark non troviamo più il protocollo
"ftp" riconosciuto nei filtri, ma cercando l'IP dell'host con cui abbiamo
comunicato: `test.rebex.net` ovvero `195.144.107.198` è possibile osservare che
la sequenza di pacchetti scambiati sono relativi al protocollo SSH, ragion per
cui possiamo affermare che le comunicazioni rispettano il protocollo `sftp`.

## Activity 3

Ignored.

## Activity 4

Ignored.
